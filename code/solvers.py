'''
solvers.py

All methods here are for computing H^-1 b, where H is the DxD Hessian and b is a
  D x N matrix.
'''

import numpy as np
import scipy.sparse.linalg



def ihvp_cholesky(b, X, D2,
                  regularizer_hessian):
  hess = (D2[:,np.newaxis]*X).T.dot(X) + regularizer_hessian
  return np.linalg.solve(hess, b)


def ihvp_agarwal(b, X, D2,
                 hessian_scaling,
                 regularizer_hessian,
                 S1=None,
                 S2=None,
                 **kwargs):
  '''
  From Agarwal et. al. "Second-order stochastic optimization for machine
     learning in linear time." 2017.
  First get a stochastic estimate of the inverse Hessian, then dot it with the
    vectors b
  Not clear that this provides good accuracy in a reasonable amount of time.
  '''
  N = X.shape[0]
  D = X.shape[1]
  if S1 is None and S2 is None:
    S1 = int(np.sqrt(N)/10)
    S2 = int(10*np.sqrt(N))

  #if self.regularization is not None:
  #  evalRegHess = autograd.hessian(self.regularization)
  #  paramsCpy = self.params.get_free().copy()
  #  regHess = evalRegHess(self.params.get_free())
  #  regHess[-1,-1] = 0.0
  #  self.params.set_free(paramsCpy)

  hinvEsts = np.zeros((S1,D,D))
  for ii in range(S1):
    hinvEsts[ii] = np.eye(D)
    for n in range(1,S2):
      idx = np.random.choice(N)
      H_n = np.outer(X[idx],X[idx]) * D2[idx] * N + regularizer_hessian

      if np.linalg.norm(H_n) >= hessian_scaling*0.9999:
        from IPython import embed; np.set_printoptions(linewidth=150); embed()
        print(np.linalg.norm(H_n))
      #H_n = self.get_single_datapoint_hessian(idx) * N
      H_n /= hessian_scaling
      hinvEsts[ii] = np.eye(D) + (np.eye(D) - H_n).dot(hinvEsts[ii])

  Hinv = np.mean(hinvEsts, axis=0) / hessian_scaling
  return Hinv.dot(b)


def ihvp_exactEvecs(b, X, D2,
                    rank=1,
                    regularizer_hessianVP=None,
                    L2Lambda=None,
                    **kwargs):
  '''
  regularizer_hessianVP should be a function taking a D-dimensional vector x
   and returning \nabla^2 R(\theta) * x. By default, assumes the regularizer
   is L2Lambda*np.linalg.norm(theta[:-1])**2 (i.e., l2 regularization on
   everything but the bias term)

  Uses the Lanczos algorithm to compute the top rank eigenvectors
  '''
  N = X.shape[0]
  D = X.shape[1] - 1
  if regularizer_hessianVP is None:
    eD1 = np.zeros(D+1)
    eD1[D] = 1.0
    regularizer_hessianVP = lambda x: 2*L2Lambda*x - 2*L2Lambda*x[-1]*eD1
    
  Hdotv = scipy.sparse.linalg.LinearOperator(
    shape=(D+1,D+1),
    matvec=lambda v, X=X, D2=D2: ( np.dot(X.T, np.dot(D2[:,np.newaxis]*X, v)) +
                       regularizer_hessianVP(v) )
    )
  evals, evecs = scipy.sparse.linalg.eigsh(Hdotv, k=rank, which='LM')
  return evecs.dot(np.diag(1/evals)).dot(evecs.T).dot(b)


def tropp_sketch(X, D2, L2Lambda, K,
                 seed=1234,
                 omegaForm='normal'):
  '''
  Helper method for ihvp_tropp
  '''
  D = X.shape[1]
  N = X.shape[0]
  
  ## Form test vectors, omega
  np.random.seed(seed)
  if omegaForm == 'normal':
    # same thing as qr of np.random.normal(size=(D,K)), but drawing the random
    #  numbers in this order ensures that, e.g. the first 5 columns of omega are
    #  the same whether K = 5 or K = 200.
    omega = np.linalg.qr(np.random.normal(size=(K,D)).T)[0]
  elif omegaForm == 'X':
    # Alternative for omega: make the test vectors relate to the data, X
    omega = np.linalg.qr(X.T.dot(np.random.normal(size=(N,K))))[0]

  ## Form the sketch
  sketch = omega * (2*L2Lambda)
  sketch[-1,:] = 0.0
  for n in range(N):
    sketch += np.outer(X[n] * D2[n], X[n].T.dot(omega))
  return sketch, omega

def tropp_summarize_sketch(sketch, omega, rank, eps=1e-8):
  nu = eps * np.linalg.norm(sketch.ravel())
  sketch += nu * omega
  B = omega.T.dot(sketch)
  C = np.linalg.cholesky((B + B.T)/2)
  E = np.linalg.solve(C, sketch.T).T # == sketch.dot(np.linalg.inv(C.T))
  U, S, V = np.linalg.svd(E,
                          full_matrices=False) #E = U.dot(np.diag(S)).dot(V)
  U = U[:,:rank]
  S = S[:rank]

  return U, S**2 - nu


def ihvp_tropp_LRDiagonal(b, X, D2,
                          L2Lambda,
                          rank=1,
                          K=None,
                          non_fixed_dims=None,
                          seed=1234,
                          **kwargs):
  if K is None:
    K = 2*rank
  sketch, omega = tropp_sketch(X, D2, L2Lambda=0.0, K=K, seed=seed)
  vecsAppx, valsAppx = tropp_summarize_sketch(sketch, omega, rank)
  goodInds = np.where(valsAppx > 1e-8)[0]
  vecsAppx = vecsAppx[:,goodInds]
  valsAppx = valsAppx[goodInds]

  mask = np.eye(X.shape[1]) 
  mask[-1,-1] = 0.0
  LamInv = np.eye(X.shape[1]) * 1./(2*L2Lambda)

  print('TODO : do not explicitly form matrices')
  Ainv = vecsAppx.dot(np.diag(1/valsAppx)).dot(vecsAppx.T)
  from IPython import embed; np.set_printoptions(linewidth=150); embed()
  mid = np.linalg.solve(LamInv + mask.dot(Ainv).dot(mask),
                        mask.dot(Ainv))
  
  hivps = ( Ainv - Ainv.dot(mask).dot(mid) )
  return hivps, 0.0


def ihvp_tropp(b, X, D2,
               L2Lambda,
               rank=1,
               K=None,
               non_fixed_dims=None,
               seed=1234,
               omegaForm='normal',
               **kwargs):
  '''
  Returns matrix with columns H^{-1}.dot(b[:,i]), where H is the Hessian
    evaluated
  Method is from Tropp et. al 2017, "Fixed-rank approximation of
    a positive-semidefinite matrix from streaming data"
  '''
  if K is None:
    K = 1*rank
  sketch, omega = tropp_sketch(X, D2, L2Lambda, K,
                               seed=seed,
                               omegaForm=omegaForm)
  vecsAppx, valsAppx = tropp_summarize_sketch(sketch, omega, rank)
  goodInds = np.where(valsAppx > 1e-8)[0]
  
  # Approximately compute H^{-1}.dot(b)
  hivps = vecsAppx[:,goodInds].dot(np.diag(1/valsAppx[goodInds])).dot(vecsAppx[:,goodInds].T).dot(b)
  
  # For diagnostic purposes, compute schatten 1 norm, ||H - \tilde H||_1
  D = X.shape[1]
  hessReg = np.eye(D) * 2 * L2Lambda
  hessReg[-1,-1] = 0.0
  hess = X.T.dot(np.diag(D2)).dot(X) + hessReg
  vals, vecs = np.linalg.eigh(hess)
  goodInds = np.where(vals > 1e-6)[0]

  schatten1 = np.abs(np.linalg.eigvalsh(vecs[:,goodInds].dot(np.diag(vals[goodInds])).dot(vecs[:,goodInds].T) - vecsAppx.dot(np.diag(valsAppx)).dot(vecsAppx.T))).sum()

  goodInds = np.where(vals > 0)[0]
  
  hivpsExact = vecs[:,goodInds].dot(np.diag(1/vals[goodInds])).dot(vecs[:,goodInds].T).dot(X.T)
  '''
  print('Total error =', ((np.linalg.norm(hivpsExact - hivps, axis=0))**2))
  print('Rank =', rank,
        'Error on =', (np.linalg.norm(hivpsExact[:10,:] - hivps[:10,:], axis=0)**2).mean(),
        'Error off =', (np.linalg.norm(hivpsExact[10:,:] - hivps[10:,:], axis=0)**2).mean())
  '''

  #from IPython import embed; np.set_printoptions(linewidth=150); embed()
  
  return hivps, schatten1

  







'''
  vals, vecs = np.linalg.eigh(hess)
  goodInds = vals > 1e-5
  actualHivps = vecs[:,goodInds].dot(np.diag(1/vals[goodInds])).dot(vecs[:,goodInds].T).dot(b)
  print('relative error', np.abs(np.linalg.eigvalsh(hess - U.dot(np.diag(S**2-nu)).dot(U.T))).sum() / vals[:-rank].sum(),
        'numer', np.abs(np.linalg.eigvalsh(hess - U.dot(np.diag(S**2-nu)).dot(U.T))).sum(),
        'denom', vals[:-rank].sum())
  print('nonrel error = ', np.linalg.norm(actualHivps - hivps, axis=0).mean())
  schatten1 = np.abs(np.linalg.eigvalsh(hess - U.dot(np.diag(S**2 - nu)).dot(U.T))).sum()
'''  
